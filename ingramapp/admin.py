from django.contrib import admin

# Register your models here.
from ingramapp.models import Photo

admin.site.register(Photo)
