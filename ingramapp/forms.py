from django import forms

from ingramapp.models import Photo


class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = "__all__"
        widgets = {
            "caption": forms.TextInput(attrs={'placeholder': 'Enter short caption'})
        }
