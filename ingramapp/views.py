from django.shortcuts import render, redirect

# Create your views here.
from ingramapp.forms import PhotoForm
from ingramapp.models import Photo


def photos(request):
    all_photos = Photo.objects.all()

    if request.method == 'POST':
        photo = PhotoForm(request.POST or None, request.FILES or None)
        if photo.is_valid():
            photo.save()
            return redirect("photos")
    else:
        photo = PhotoForm()

    context = {
        'photos': all_photos,
        'photo_form': photo
    }

    return render(request, 'index.html', context)
