from django.apps import AppConfig


class IngramappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ingramapp'
