from django.urls import path

from ingramapp import views

urlpatterns = [
    path('', views.photos, name="photos")
]