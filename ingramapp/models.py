from django.db import models


# Create your models here.
class Photo(models.Model):
    image = models.ImageField(verbose_name="", unique=True)
    caption = models.CharField(max_length=255, verbose_name="")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.image.name
